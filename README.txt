SpinInt for Drupal 7

Spinint provides a simple up down arrow widget for integer number fields.

Install as usual either download and unzip into sites/all/module or download 
using drush dl spinint.

Enable through modules page, or using drush.

Define a new integer field, choose Spinning integer for the widget type.
